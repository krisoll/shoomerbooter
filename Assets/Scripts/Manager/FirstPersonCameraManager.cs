﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstPersonCameraManager : MonoBehaviour
{
    [SerializeField]
    Transform character;
    public float sensitivity = 1;
    public float smoothing = 2;
    [HideInInspector]
    public float MouseMovementX;
    [HideInInspector]
    public float MouseMovementY;
    public static FirstPersonCameraManager Instance;
    private List<CameraFacer> cameraFacers = new List<CameraFacer>();
    private Vector2 currentMouseLook;
    private Vector2 appliedMouseDelta;
    private Coroutine shakeCoroutine;
    private Vector3 initialPos;
    [SerializeField]
    private float swayAmount;
    [SerializeField]
    private float smoothAmount;
    [SerializeField]
    private float bobSpeed;
    [SerializeField]
    private float bobDistance;
    private float waveSlice;
    private float timer;
    public void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
        initialPos = transform.localPosition;
    }
    private void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }
    void Reset()
    {
        character = GetComponentInParent<FirstPersonMovement>().transform;
    }
    private void FixedUpdate()
    {
        //Face sprites to camera
        foreach(CameraFacer c in cameraFacers)
        {
            if (c == null)
            {
                cameraFacers.Remove(c);
                break;
            }
            c.FaceCamera(transform);
        }
        // Rotate camera and controller.
        transform.localRotation = Quaternion.AngleAxis(-currentMouseLook.y, Vector3.right);
        character.localRotation = Quaternion.AngleAxis(currentMouseLook.x, Vector3.up);
    }
    private void Update()
    {
        // Get smooth mouse look.
        Vector2 smoothMouseDelta = Vector2.Scale(new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y")), Vector2.one * sensitivity * smoothing);
        appliedMouseDelta = Vector2.Lerp(appliedMouseDelta, smoothMouseDelta, 1 / smoothing);
        currentMouseLook += appliedMouseDelta;
        currentMouseLook.y = Mathf.Clamp(currentMouseLook.y, -90, 90);
        MouseMovementX = Input.GetAxis("Mouse X");
        MouseMovementY = Input.GetAxis("Mouse Y");
    }
    public void AddCameraFacer(CameraFacer facer)
    {
        cameraFacers.Add(facer);
    }

    public void Shake(float amplitude, float velocity, float duration)
    {
        if (shakeCoroutine != null)
        {
            StopCoroutine(shakeCoroutine);
            transform.localPosition = initialPos;
        }
        shakeCoroutine = StartCoroutine(ShakeCoroutine(amplitude,velocity,duration));
    }
    IEnumerator ShakeCoroutine(float amplitude, float velocity, float duration)
    {
        float counter = 0;
        float currentDirection = 1;
        float fCameraPos = transform.localPosition.y + amplitude;
        float f2CameraPos = transform.localPosition.y- amplitude;
        while (counter<duration)
        {
            if (transform.localPosition.y > fCameraPos && currentDirection == 1) currentDirection = -1;
            if (transform.localPosition.y < f2CameraPos && currentDirection == -1) currentDirection = 1;
            transform.localPosition = Vector3.MoveTowards(transform.localPosition, 
                transform.localPosition + Vector3.up * currentDirection, Time.deltaTime * velocity);
            counter += Time.deltaTime;
            yield return null;
        }
        transform.localPosition = initialPos;
    }
}
