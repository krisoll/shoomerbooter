﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using System.IO;
public class DataManager : MonoBehaviour
{
    public static DataManager Instance;
    [HideInInspector]
    public PlayerDataController playerData;
    public Dictionary<string,Weapon> weaponData = new Dictionary<string, Weapon>();
    private void Awake()
    {
        LoadWeaponData();
        if (Instance == null) Instance = this;
    }
    public void LoadWeaponData()
    {
        TextAsset json = Resources.Load<TextAsset>("Data/weapons");
        var data = JsonConvert.DeserializeObject<WeaponData>(json.text);
        foreach(Weapon w in data.data)
        {
            weaponData.Add(w.name, w);
        }
    }
    public void SaveGameData()
    {

    }
    public void LoadGameData()
    {

    }
    public void LoadOptions()
    {

    }
    public void SaveOptions()
    {

    }
    public void SetPlayerData(PlayerDataController controller)
    {
        playerData = controller;
    }
    [System.Serializable]
    public class Weapon
    {
        public string name;
        public int maxAmmo;
        public int ammoId;
        public int weaponSlot;
    }
    [System.Serializable]
    public class WeaponData
    {
        public List<Weapon> data;
    }


}
