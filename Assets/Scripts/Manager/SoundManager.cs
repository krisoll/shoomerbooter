﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public static SoundManager Instance;
    public int effectSourcesNum;
    public int voiceSourcesNum;
    private AudioSource musicSource;
    private List<AudioSource> effectSources = new List<AudioSource>();
    private List<AudioSource> voiceSources = new List<AudioSource>();
    private bool initialized;
    // Start is called before the first frame update
    void Awake()
    {
        if (Instance == null) Instance = this;
    }
    private void Start()
    {
        if (!initialized) Initialize();
    }
    private void Initialize()
    {
        GameObject audio = new GameObject("Audio");
        audio.transform.SetParent(this.transform);
        GameObject voices = new GameObject("Voices");
        GameObject effects = new GameObject("Effects");
        voices.transform.SetParent(audio.transform);
        effects.transform.SetParent(audio.transform);

        //Spawn Music Source
        GameObject music = new GameObject("Music");
        music.transform.SetParent(audio.transform);
        musicSource = music.AddComponent<AudioSource>();
        musicSource.volume = 0.5f;

        //Spawn Effects Sources
        for (int i = 0; i < effectSourcesNum; i++)
        {
            GameObject g = new GameObject("EffectSource " + i);
            g.transform.SetParent(effects.transform);
            effectSources.Add(g.AddComponent<AudioSource>());
        }

        //Spawn VoiceSources
        for (int i = 0; i < voiceSourcesNum; i++)
        {
            GameObject g = new GameObject("VoiceSource " + i);
            g.transform.SetParent(voices.transform);
            voiceSources.Add(g.AddComponent<AudioSource>());
        }

        initialized = true;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="clip">Clip to be played</param>
    /// <param name="loops">Clip Loops?</param>
    /// <returns></returns>
    public AudioSource PlayMusic(AudioClip clip, bool loops = true)
    {

        musicSource.clip = clip;
        musicSource.loop = loops;
        musicSource.Play();
        return musicSource;
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="clip">Clip to be played</param>
    /// <param name="loops">Clip Loops?</param>
    /// <returns></returns>
    public AudioSource PlaySoundEffect(AudioClip clip, bool loops = false)
    {
        foreach(AudioSource source in effectSources)
        {
            if (!source.isPlaying)
            {
                source.clip = clip;
                source.loop = loops;
                source.Play();
                return source;
            }
        }
        return null;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="clip">Clip to be played</param>
    /// <param name="loops">Clip Loops?</param>
    /// <returns></returns>
    public AudioSource PlaySoundVoice(AudioClip clip, bool loops = false)
    {
        foreach (AudioSource source in voiceSources)
        {
            if (!source.isPlaying)
            {
                source.clip = clip;
                source.loop = loops;
                source.Play();
                return source;
            }
        }
        return null;
    }
}
