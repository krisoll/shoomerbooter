﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileManager : MonoBehaviour
{
    public static ProjectileManager Instance;
    private Dictionary<string, ProjectileList> projectiles = new Dictionary<string, ProjectileList>();

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
    }
    public void InstantiateProjectile(Vector3 pos, ProjectileData data, Quaternion rotation)
    {
        GameObject ProjInstance = null;
        if (projectiles.ContainsKey(data.name))
        {
            //List of projectiles exists and searchs for available projectiles , if none is available, create a new ProjectileInstance
            List<GameObject> gameObjs = projectiles[data.name].projectiles;
            foreach (GameObject g in gameObjs)
            {
                if (!g.activeInHierarchy)
                {
                    ProjInstance = g;
                    break;
                }
            }
            if(ProjInstance==null)
            {
                //Creates new Projectiles Instance
                ProjInstance = Instantiate(data.projectilePrefab, projectiles[data.name].parent.transform);
            }

        }
        else
        {
            //Create a new List and Create new Projectile
            GameObject g = new GameObject(data.name);
            g.transform.SetParent(transform);
            ProjectileList list = new ProjectileList();
            list.parent = g;
            projectiles.Add(data.name,list);
            ProjInstance = Instantiate(data.projectilePrefab, projectiles[data.name].parent.transform);
            ProjInstance.transform.position = pos;
        }

        //Set Position and enable
        ProjInstance.transform.rotation = rotation;
        ProjInstance.transform.position = pos;
        ProjInstance.SetActive(true);
    }
    [System.Serializable]
    public class ProjectileList
    {
        public GameObject parent;
        public List<GameObject> projectiles = new List<GameObject>();

    }
}
