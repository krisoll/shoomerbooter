﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DecalManager : MonoBehaviour
{
    public static DecalManager Instance;
    public int maxDecals;
    public int decalMaxTimer;
    private List<DecalInstance> decals = new List<DecalInstance>();
    private void Awake()
    {
        if (Instance == null) Instance = this;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="decal">Decal Prefab</param>
    /// <param name="position">Position the decal will spawn</param>
    /// <param name="rotation">Rotation the decal will Face</param>
    public void SpawnDecal(GameObject decalPrefab, Vector3 position, Quaternion rotation,bool randomRotation = false)
    {
        GameObject g = Instantiate(decalPrefab, position, rotation);
        if (randomRotation)
        {
            g.transform.eulerAngles = new Vector3(g.transform.eulerAngles.x, g.transform.eulerAngles.y, Random.Range(-180, 180));
        }
        g.transform.SetParent(transform);
        g.name = "Decal " + decals.Count;
        DecalInstance d = new DecalInstance(g);
        decals.Add(d);
    }
    [System.Serializable]
    public class DecalInstance
    {
        public GameObject decal;
        public float timer;
        public DecalInstance(GameObject decal)
        {
            this.decal = decal;
        }
    }
}
