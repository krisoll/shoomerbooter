﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDataController : MonoBehaviour
{
    public int currentHealth;
    public int currentShields;
    public int currentWeapon;
    public Dictionary<string, PlayerWeaponData> weaponData = new Dictionary<string, PlayerWeaponData>();
    private void Start()
    {
        DataManager.Instance.SetPlayerData(this);
    }
    [SerializeField]
    public class PlayerWeaponData
    {
        public string name;
        public int ammoId;
        public int maxAmmo;
        public int currentAmmo;
    }
}
