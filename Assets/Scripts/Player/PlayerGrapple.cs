﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerGrapple : MonoBehaviour
{
    public FirstPersonMovement movementController;
    public float FowardVelocity;
    public float BackWardVelocity;
    public float MaxDistance;
    public Material lineMaterial;
    public float lineWidth;
    public LayerMask mapLayer;
    public Action<Vector3,Vector3,Hookable> HookCallback;
    public Action OnReturned;
    public Action OnLaunched;
    private bool foward;
    private bool launched;
    private bool hooked;
    private Transform initialParent;
    private Vector3 finalPos;
    private LineRenderer line;
    private void Start()
    {
        initialParent = transform.parent;
    }
    private void OnDrawGizmos()
    {
        Gizmos.DrawSphere(finalPos, 0.3f);
    }
    private void LateUpdate()
    {
        if (line != null)
        {
            line.SetPosition(0, initialParent.position);
            line.SetPosition(1, transform.position);
        }
        if (launched)
        {
            if (foward)
            {
                if (Vector3.Distance(transform.position, finalPos)<0.01f)
                {
                    foward = false;
                }
                MoveFoward();
            }
            else
            {
                if (Vector3.Distance(transform.position, initialParent.position) < 0.01f)
                {
                    ReturnHand();
                }
                MoveBackWard();
            }
        }
    }
    public void LaunchGrapple()
    {
        if (!launched)
        {
            transform.SetParent(null);
            launched = true;
            foward = true;
            finalPos = transform.position + (transform.forward * MaxDistance);
            if (line == null) line = gameObject.AddComponent<LineRenderer>();
            line.material = lineMaterial;
            line.startWidth = lineWidth;
            line.endWidth = lineWidth;
            line.useWorldSpace = true;
            line.textureMode = LineTextureMode.Tile;
            OnLaunched?.Invoke();
        }
    }
    public void ForceReturnHand()
    {
        launched = true;
        foward = false;
    }
    public void ReturnHand()
    {
        transform.SetParent(initialParent);
        transform.localEulerAngles = Vector3.zero;
        launched = false;
        OnReturned?.Invoke();
    }
    void MoveFoward()
    {
        transform.position = Vector3.MoveTowards(transform.position, finalPos, FowardVelocity * Time.deltaTime);
    }
    void MoveBackWard()
    {
        transform.position = Vector3.MoveTowards(transform.position, initialParent.position, BackWardVelocity * Time.deltaTime);
    }
    private void OnTriggerEnter(Collider other)
    {
        //Debug.Log("Trigger Collided with " + other.name);
        Hookable hook = other.GetComponent<Hookable>();
        if (hook != null)
        {
            hooked = true;
            HookCallback(hook.GetHookableDirection(), hook.GetHookablePosition(),hook);
            launched = false;
            transform.position = hook.GetHookablePosition();
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if ((((1 << other.gameObject.layer) & mapLayer) != 0)&& !hooked) ForceReturnHand();
    }
}
