﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstPersonFunctionCaller : MonoBehaviour
{
    public FirstPersonMovement movementController;
    public PlayerAnimator anim;
    public void LaunchHook()
    {
        movementController.LaunchGrapple();
    }
}
