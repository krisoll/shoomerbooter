﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstPersonShootController : MonoBehaviour
{
    public Animator gunAnimator;
    public RayCaster gunCaster;
    public GameObject bulletHole;
    public int damage;
    public CameraShaker shaker;
    public LayerMask enemies;
    public float shakeAmplitude = 0.01f;
    public float shakeVelocity = 4f;
    public float shakeDuration = 0.15f;
    private int currentWeapon;
    // Start is called before the first frame update
    void Start()
    {
        SetWeapon(1);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            gunAnimator.SetTrigger("Fire");
            FireWeapon();
        }
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            SetWeapon(0);
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            SetWeapon(1);
        }
    }

    void SetWeapon(int num)
    {
        currentWeapon = num;
        gunAnimator.SetInteger("Gun", num);
        gunAnimator.SetTrigger("ChangeWeapon");
    }
    void CastGrapple()
    {
        
    }
    void FireWeaponWithDelay()
    {
        gunCaster.Cast(GunCallback);
        shaker.Shake(shakeAmplitude, shakeVelocity, shakeDuration);
    }

    void FireWeapon()
    {
        gunCaster.Cast(GunCallback);
        shaker.Shake(shakeAmplitude, shakeVelocity, shakeDuration);
    }

    void GunCallback(RaycastHit hit,Vector3 origin)
    {
        if (enemies == (enemies | (1 << hit.collider.gameObject.layer)))
        {
            var reciever = hit.collider.GetComponent<DamageReciever>();
            if (reciever != null)
            {
                reciever.Damage(damage, hit.point, origin);
            }
        }
        else
        {
            Vector3 decalPos = hit.point + (hit.normal * 0.011f);
            Quaternion rotation = Quaternion.LookRotation(-hit.normal, Vector3.up);
            DecalManager.Instance.SpawnDecal(bulletHole, decalPos, rotation, true);
        }
    }
}
