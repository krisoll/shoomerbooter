﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShaker : MonoBehaviour
{
    [ContextMenu("Shake")]
    public void Shake(float amplitude, float velocity, float duration)
    {
        FirstPersonCameraManager.Instance.Shake(amplitude, velocity, duration);
    }
}
