﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public PlayerGrapple grapple;
    public PlayerAnimator animator;
    public FirstPersonMovement movement;
    public void Start()
    {
        grapple.OnReturned += animator.ReturnHand;
    }
}
