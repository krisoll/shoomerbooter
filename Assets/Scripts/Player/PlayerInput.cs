﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Rewired;
public class PlayerInput : MonoBehaviour
{
    private Player rePlayer;
    public Vector2 MoveAxis;
    public Action OnPrimary = delegate { };
    public Action OnSecondary = delegate { };
    public Action OnDash = delegate { };
    public Action OnGrapple = delegate { };
    public List<Action> OnChangeWeapon;
    // Start is called before the first frame update
    void Awake()
    {
        rePlayer = ReInput.players.GetPlayer(0);
        OnChangeWeapon = new List<Action>();
        for(int i = 0; i < 9; i++)
        {
            OnChangeWeapon.Add(delegate { });
        }
    }

    // Update is called once per frame
    void Update()
    {
        MoveAxis = new Vector2(rePlayer.GetAxis("Horizontal"), rePlayer.GetAxis("Vertical"));
        if (rePlayer.GetButtonDown("Dash")) OnDash();
        if (rePlayer.GetButtonDown("Primary")) OnPrimary();
        if (rePlayer.GetButtonDown("Secondary")) OnSecondary();
        if (rePlayer.GetButtonDown("Weapon1")) OnChangeWeapon[0]();
        if (rePlayer.GetButtonDown("Weapon2")) OnChangeWeapon[1]();
        if (rePlayer.GetButtonDown("Weapon3")) OnChangeWeapon[2]();
        if (rePlayer.GetButtonDown("Weapon4")) OnChangeWeapon[3]();
        if (rePlayer.GetButtonDown("Weapon5")) OnChangeWeapon[4]();
        if (rePlayer.GetButtonDown("Weapon6")) OnChangeWeapon[5]();
        if (rePlayer.GetButtonDown("Weapon7")) OnChangeWeapon[6]();
        if (rePlayer.GetButtonDown("Weapon8")) OnChangeWeapon[7]();
        if (rePlayer.GetButtonDown("Grapple")) OnGrapple();
    }
}
