﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimator : MonoBehaviour
{
    public Animator fpsAnim;
    public Animator tpsAnim;
    public void LaunchHook()
    {
        fpsAnim.SetTrigger("LaunchHand");
    }
    public void ReturnHand()
    {
        fpsAnim.SetTrigger("ReattachHand");
    }
}
