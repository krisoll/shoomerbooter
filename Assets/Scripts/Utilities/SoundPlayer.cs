﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundPlayer : MonoBehaviour
{
    public List<AudioClip> clips;

    public void PlayClip(int index)
    {
        SoundManager.Instance.PlaySoundEffect(clips[index]);
    }
}
