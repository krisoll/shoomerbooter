﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileShooter : MonoBehaviour
{
    public List<ProjectileData> projectiles;
    public void ShootProjectile(int projectile)
    {
        ProjectileManager.Instance.InstantiateProjectile(transform.position, projectiles[projectile],transform.rotation);
    }
}
