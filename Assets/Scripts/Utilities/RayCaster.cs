﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class RayCaster : MonoBehaviour
{
    public void Cast(Action<RaycastHit,Vector3> callback, Action failCallback = null)
    {
        Ray ray = new Ray(transform.position, transform.forward);
        RaycastHit rayInfo = new RaycastHit();
        if (Physics.Raycast(ray, out rayInfo))
        {
            callback(rayInfo, transform.position);
        }
    }
}
