﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageReciever : MonoBehaviour
{
    public int maxHealth = 100;
    public int currentHealth;
    public Action onDie = delegate{ };
    public bool dead;
    public ParticleSystem particle;
    private ParticleSystem.EmissionModule emission;
    private void Start()
    {
        currentHealth = maxHealth;
        emission = particle.emission;
    }
    public void Damage(int damage,Vector3 hitPosition, Vector3 origin)
    {
        if (particle != null)
        {
            ParticleSystem.Burst burst = emission.GetBurst(0);
            burst.count = Mathf.RoundToInt(damage / 5);
            particle.time = 0;
            emission.enabled = true;
            particle.transform.position = hitPosition;
            particle.transform.LookAt(origin);
        }
        if (!dead)
        {
            currentHealth -= damage;
            if (currentHealth <= 0)
            {
                Die();
            }
        }
    }

    void Die()
    {
        if (particle != null)
        {
            emission.enabled = false;
        }
        dead = true;
        onDie();
    }
}
