﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicPlayer : MonoBehaviour
{
    public AudioClip[] music;
    public KeyCode[]  nextSongKey = new KeyCode[]{KeyCode.P};
    private int currentSong;
    // Start is called before the first frame update
    IEnumerator Start()
    {
        yield return new WaitUntil(() => SoundManager.Instance != null);
        SoundManager.Instance.PlayMusic(music[0]);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(nextSongKey[0]))
        {
            currentSong++;
            if (currentSong >= music.Length) currentSong = 0;
            SoundManager.Instance.PlayMusic(music[currentSong]);
        }
    }
}
