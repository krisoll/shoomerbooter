﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hookable : MonoBehaviour
{
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.cyan;
        Gizmos.DrawLine(transform.position, transform.position+transform.right);
    }
    public Vector3 GetHookableDirection()
    {
        return transform.right;
    }
    public Vector3 GetHookablePosition()
    {
        return transform.position;
    }
}
