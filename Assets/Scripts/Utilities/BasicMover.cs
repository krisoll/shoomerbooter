﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicMover : MonoBehaviour
{
    public float velocity;
    private Vector3 direction;
    private void Start()
    {
        SetDirection(transform.forward);
    }
    public void SetDirection(Vector3 direction)
    {
        this.direction = direction;
    }

    private void FixedUpdate()
    {
        transform.position = Vector3.MoveTowards(transform.position, transform.position + direction, velocity * Time.deltaTime);
    }
}
