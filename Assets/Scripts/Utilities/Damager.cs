﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Damager : MonoBehaviour
{
    public int damage;
    public LayerMask damageMask;
    public Action OnDamage = delegate { };
    public Action OnCollide = delegate { };

    public void Damage(DamageReciever reciever)
    {
        OnDamage();
        reciever.Damage(damage, Vector3.zero, Vector3.zero);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (damageMask == (damageMask | (1 << other.gameObject.layer)))
        {
            OnCollide();
            DamageReciever rec = other.GetComponent<DamageReciever>();
            if (rec != null) Damage(rec);
            Destroy(this.gameObject);
        }
    }
}
