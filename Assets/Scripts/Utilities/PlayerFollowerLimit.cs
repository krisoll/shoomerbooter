﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFollowerLimit : MonoBehaviour
{

    public float maxAngle;
    private Vector3 initialEulers;
    public float currentAngle;
    private void Start()
    {
        initialEulers = transform.eulerAngles;
    }
    private void Update()
    {
        Vector3 direction = GameManager.Instance.player.transform.position - transform.position;
        Quaternion lookRotation = Quaternion.LookRotation(direction);
        float angle = Quaternion.Angle(transform.parent.rotation, lookRotation);
        if (Mathf.Abs(angle) < maxAngle)
        {
            transform.LookAt(GameManager.Instance.player.transform);
        }
        currentAngle = angle;

    }
}
