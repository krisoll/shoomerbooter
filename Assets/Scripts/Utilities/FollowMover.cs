﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowMover : MonoBehaviour
{
    public float velocity;
    public float rotateVelocity;
    private Transform target;
    private void Start()
    {
        //TODO delete this
        SetTarget(GameManager.Instance.player.transform);
    }
    public void SetTarget(Transform direction)
    {
        this.target = direction;
    }

    private void FixedUpdate()
    {
        var targetRotation = Quaternion.LookRotation(target.position - transform.position);
        Quaternion.RotateTowards(transform.rotation, targetRotation, rotateVelocity * Time.fixedDeltaTime);
        transform.position = Vector3.MoveTowards(transform.position, transform.position + transform.forward, velocity * Time.fixedDeltaTime);
    }
}
