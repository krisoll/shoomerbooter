﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFacer : MonoBehaviour
{
    public bool lockY;
    private void Start()
    {
        FirstPersonCameraManager.Instance.AddCameraFacer(this);
    }
    public void FaceCamera(Transform cameraPoint)
    {
        if (lockY)
        {
            transform.LookAt(new Vector3(cameraPoint.position.x,transform.position.y,cameraPoint.position.z));
        }
        else
        {
            transform.LookAt(cameraPoint);
        }
    }
}
