﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class BasicEnemy : MonoBehaviour
{
    public Animator anim;
    public NavMeshAgent agent;
    public Rigidbody rigid;
    public DamageReciever reciever;
    public float roamMaxRadius;
    public float roamMinRadius;
    public float rotationSpeed = 120;
    public float maximumSpread = 120;
    public float remainingDistance;
    public ProjectileShooter cannon1, cannon2;
    private Coroutine roamCoroutine;
    private void Start()
    {
        reciever.onDie += Die;
        roamCoroutine = StartCoroutine(RoamCoroutine());
    }

    public Vector3 GetRandomRoamPos()
    {
        float randomX = Random.Range(-roamMaxRadius, roamMaxRadius);
        float randomZ = Random.Range(-roamMaxRadius, roamMaxRadius);
        Vector3 playerPos = GameManager.Instance.player.transform.position;
        Vector3 pos = new Vector3(randomX + playerPos.x, playerPos.y, randomZ + playerPos.z);
        return pos;
    }
    void Die()
    {
        anim.SetTrigger("Die");
        agent.enabled = false;
        this.enabled = false;
        rigid.isKinematic = true;
        GetComponent<Collider>().enabled = false;
        StopAllCoroutines();
    }
    IEnumerator RoamCoroutine()
    {
        agent.isStopped = false;
        agent.SetDestination(GetRandomRoamPos());
        anim.SetBool("Walking", true);
        yield return new WaitUntil(() => agent.isStopped || agent.remainingDistance <= 0.5f);
        agent.isStopped = true;
        agent.SetDestination(GameManager.Instance.player.transform.position);
        anim.SetBool("Walking", false);
        yield return StartCoroutine(RotateTowardsPlayer());
        yield return StartCoroutine(FireToPlayer());
        yield return new WaitForSeconds(1);
        roamCoroutine = StartCoroutine(RoamCoroutine());
    }
    IEnumerator RotateTowardsPlayer()
    {
        Vector3 direction = (GameManager.Instance.player.transform.position - transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(direction);
        while (!Mathf.Approximately(lookRotation.eulerAngles.y, transform.rotation.eulerAngles.y))
        {
            transform.rotation = Quaternion.RotateTowards(transform.rotation, lookRotation, Time.deltaTime * rotationSpeed);
            yield return null;
        }
    }
    void ShoootToPlayer()
    {
        cannon1.ShootProjectile(0);
        cannon2.ShootProjectile(0);
    }
    IEnumerator FireToPlayer()
    {
        anim.SetTrigger("Fire");
        yield return new WaitForSeconds(2);
    }
    private void Update()
    {
        remainingDistance = agent.remainingDistance;
    }
}
