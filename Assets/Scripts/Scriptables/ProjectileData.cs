﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "New Projectile", menuName = "Scriptables/Projectile")]
public class ProjectileData : ScriptableObject
{
    public GameObject projectilePrefab;
    public int projectileDamage;
}
