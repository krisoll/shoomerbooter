﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "New Ammo Type", menuName = "Scriptables/Ammo")]

public class ScriptableAmmo : ScriptableObject
{
    string id;
}
