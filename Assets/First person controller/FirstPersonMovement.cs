﻿using UnityEngine;
using System;
using System.Collections;
public class FirstPersonMovement : MonoBehaviour
{
    public Rigidbody rigid;
    //public FirstPersonCameraManager cameraManager;
    public PlayerAnimator anim;
    public float speed = 5;
    public float runSpeed = 10;
    public float dashSpeed = 20;
    public float balanceForce = 100;
    public float dashDuration = 0.2f;
    public PlayerGrapple GrappleHand;
    public float hookDistance;
    public float moveToHookVelocity;
    private bool running;
    private Vector3 velocity;
    private bool dashing;
    //Hook Variables
    private bool hooked;
    private bool movingToHookedPosition;
    private Vector3 hookPosition;
    private Vector3 hookDirection;
    private Hookable hookedObject;
    private Vector3 normalizedDirection;
    private Vector3 moveVector;
    private Vector3 dashVector;
    private SpringJoint hookJoint;
    private PlayerInput input;
    private void Start()
    {
        input = GetComponent<PlayerInput>();
        input.OnDash += Dash;
        input.OnGrapple += OnGrapple;
        GrappleHand.HookCallback = Hooked;
    }
    public void OnGrapple()
    {
        anim.LaunchHook();
    }
    public void LaunchGrapple()
    {
        GrappleHand.LaunchGrapple();
    }
    void Hooked(Vector3 hookDirection, Vector3 hookPosition,Hookable hook)
    {
        hookDirection = Vector3.Normalize(transform.position - hookPosition);
        this.hookPosition = hookPosition + (hookDirection * hookDistance);
        hookedObject = hook;
        hooked = true;
        movingToHookedPosition = true;
        rigid.isKinematic = true;
    }
    void Dash()
    {
        UnHook();
        if (!dashing) StartCoroutine(DashCoroutine());
    }

    IEnumerator DashCoroutine()
    {
        dashVector = moveVector;
        dashing = true;
        yield return new WaitForSeconds(dashDuration);
        dashing = false;
    }
    public void UnHook()
    {
        if (hookJoint != null)
        {
            movingToHookedPosition = false;
            rigid.isKinematic = false;
            hooked = false;
            GrappleHand.ForceReturnHand();
            //var grapplePoint = hookedObject.GetHookablePosition();
            Destroy(hookJoint);
        }
    }
    void FixedUpdate()
    {
        if (hooked)
        {
            if (movingToHookedPosition)
            {
                transform.position = Vector3.MoveTowards(transform.position, hookPosition, moveToHookVelocity * Time.deltaTime);
                if (Vector3.Distance(transform.position, hookPosition) < 0.01f)
                {
                    movingToHookedPosition = false;
                    rigid.isKinematic = false;

                    //hooked = false;
                    //hookedObject.HookToObject(rigid);

                    var grapplePoint = hookedObject.GetHookablePosition();
                    hookJoint = gameObject.AddComponent<SpringJoint>();
                    hookJoint.autoConfigureConnectedAnchor = false;
                    hookJoint.connectedAnchor = grapplePoint;

                    float distanceFromPoint = Vector3.Distance(transform.position, grapplePoint);

                    //The distance grapple will try to keep from grapple point. 
                    hookJoint.maxDistance = distanceFromPoint * 0.8f;
                    hookJoint.minDistance = distanceFromPoint * 0.25f;

                    //Adjust these values to fit your game.
                    hookJoint.spring = 4.5f;
                    hookJoint.damper = 7f;
                    hookJoint.massScale = 4.5f;
                }
            }
            else
            {
                //Balancing
                if (Mathf.Abs(input.MoveAxis.x) + Mathf.Abs(input.MoveAxis.y) > 1)
                {
                    normalizedDirection = Vector3.Normalize(new Vector3(input.MoveAxis.x, 0, input.MoveAxis.y));
                }
                else
                {
                    normalizedDirection = new Vector3(input.MoveAxis.x, 0, input.MoveAxis.y);
                }
                moveVector = transform.forward * normalizedDirection.z + transform.right * normalizedDirection.x;
                Vector3 finalVelocity = moveVector * balanceForce;
                rigid.AddForce(finalVelocity);
            }
        }
        else
        {
            float currentSpeed = (running) ? runSpeed : speed;
            if (Mathf.Abs(input.MoveAxis.x) + Mathf.Abs(input.MoveAxis.y) > 1)
            {
                normalizedDirection = Vector3.Normalize(new Vector3(input.MoveAxis.x, 0, input.MoveAxis.y));
            }
            else
            {
                normalizedDirection = new Vector3(input.MoveAxis.x, 0, input.MoveAxis.y);
            }
            moveVector = transform.forward * normalizedDirection.z + transform.right * normalizedDirection.x;
            Vector3 finalVelocity = moveVector * currentSpeed;
            if (!dashing)
            {
                rigid.velocity = new Vector3(finalVelocity.x, rigid.velocity.y, finalVelocity.z);
            }
            else
            {
                Vector3 dashingVelocity = moveVector * dashSpeed;
                rigid.velocity = new Vector3(dashingVelocity.x, 0, dashingVelocity.z);
            }
        }
    }
}
