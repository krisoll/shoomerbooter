﻿using UnityEngine;

public class Jump : MonoBehaviour
{
    [SerializeField]
    GroundCheck groundCheck;
    Rigidbody rigidbody;
    public float jumpStrength = 2;
    public event System.Action Jumped;
    public int maxJumpNumber = 2;
    private int currentJump = 0;
    private FirstPersonMovement fpsController;
    void Reset()
    {
        groundCheck = GetComponentInChildren<GroundCheck>();
        if (!groundCheck)
            groundCheck = GroundCheck.Create(transform);
    }

    void Awake()
    {
        rigidbody = GetComponent<Rigidbody>();
    }
    private void Start()
    {
        groundCheck.onGrounded += Grounded;
        groundCheck.onUngrounded += UnGrounded;
        fpsController = GetComponent<FirstPersonMovement>();
    }
    void Grounded()
    {
        currentJump = 0;
    }
    void UnGrounded()
    {
        currentJump = 1;
    }
    void LateUpdate()
    {
        if (Input.GetButtonDown("Jump") && currentJump < maxJumpNumber)
        {
            fpsController.UnHook();
            rigidbody.velocity = new Vector3(rigidbody.velocity.x, 0, rigidbody.velocity.z);
            currentJump++;
            rigidbody.AddForce(Vector3.up * 100 * jumpStrength);
            Jumped?.Invoke();
        }
    }
}
